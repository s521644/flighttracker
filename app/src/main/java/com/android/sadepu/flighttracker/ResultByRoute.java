package com.android.sadepu.flighttracker;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.sadepu.flighttracker.model.Airline;
import com.android.sadepu.flighttracker.model.Flight;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class ResultByRoute extends ActionBarActivity {
    ArrayList<Flight> flights;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final SimpleDateFormat frmt = new SimpleDateFormat("hh:mm aaa");
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_by_route);


        final Intent intent = getIntent();
        flights = (ArrayList<Flight>) intent.getSerializableExtra("flights");
        ListView byRouteLV = (ListView) findViewById(R.id.byRouteLV);
        final LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View headerView = inflater.inflate(R.layout.result_by_route_header, null);

        final float density = getResources().getDisplayMetrics().density;
        final int width = Math.round(30 * density);
        final int height = Math.round(24 * density);
        final Drawable sourceicondrawable = getResources().getDrawable(R.drawable.sourceicon);
        final Drawable destinationicondrawable = getResources().getDrawable(R.drawable.destinationicon);

        sourceicondrawable.setBounds(0, 0, width, height);
        destinationicondrawable.setBounds(0, 0, width, height);

        TextView srcTV = (TextView) headerView.findViewById(R.id.srcTV);
        srcTV.setText(flights.get(0).getDepartureAirport().toString());
        srcTV.setCompoundDrawables(sourceicondrawable, null, null, null);
        TextView destTV = (TextView) headerView.findViewById(R.id.destTV);
        destTV.setText(flights.get(0).getArrivalAirport().toString());
        destTV.setCompoundDrawables(destinationicondrawable, null, null, null);
        TextView dateByRoute = (TextView) headerView.findViewById(R.id.dateByRoute);
        dateByRoute.setText(dateFormat.format(flights.get(0).getDepartureDate().getTime()));
        byRouteLV.addHeaderView(headerView, null, false);

        byRouteLV.setAdapter(new ArrayAdapter<Flight>(this, R.layout.result_by_route_list_item, R.id.airline_list_item, flights) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View listview = inflater.inflate(R.layout.result_by_route_list_item, null);
                TextView airline = (TextView) listview.findViewById(R.id.airline_list_item);
                Flight item = getItem(position);
                airline.setText(item.getAirline().toString() + " " + item.getFlightNumber());

                TextView depiconTV = (TextView) listview.findViewById(R.id.depiconTV);
                depiconTV.setText(frmt.format(item.getDepartureDate().getTime()));
                TextView arriconTV = (TextView) listview.findViewById(R.id.arriconTV);
                arriconTV.setText(frmt.format(item.getArrivalDate().getTime()));

                final Drawable depicon = getResources().getDrawable(R.drawable.depicon);
                final Drawable arricon = getResources().getDrawable(R.drawable.arricon);

                depicon.setBounds(0, 0, width, height);
                arricon.setBounds(0, 0, width, height);


                depiconTV.setCompoundDrawables(depicon, null, null, null);
                arriconTV.setCompoundDrawables(arricon, null, null, null);

                TextView delayed1TV = (TextView) listview.findViewById(R.id.delayed1TV);
                Calendar departureDate = item.getDepartureDate();
                Calendar estimatedGateDeparture = item.getEstimatedGateDeparture();
                Calendar actualGateDeparture = item.getActualGateDeparture();
                Log.d("DATES DEBUG", "Departure Date - " + frmt.format(departureDate.getTime()));

                if (estimatedGateDeparture != null) {
                    Log.d("DATES DEBUG", "Estimated Departure Date - " + frmt.format(estimatedGateDeparture.getTime()));
                    long diff = estimatedGateDeparture.getTime().getTime() - departureDate.getTime().getTime();
                    long diffMinutes = diff / (60 * 1000) % 60;
                    if (diffMinutes > 0) {
                        delayed1TV.setVisibility(View.VISIBLE);
                    }
                } else if (actualGateDeparture != null) {
                    Log.d("DATES DEBUG", "Actual Departure Date - " + frmt.format(actualGateDeparture.getTime()));
                }
                return listview;
            }
        });

        byRouteLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView delayed1TV = (TextView) view.findViewById(R.id.delayed1TV);
                Flight flight = flights.get(position - 1);
                Intent overviewIntent = new Intent(getApplicationContext(), Overview.class);
                overviewIntent.putExtra("flight", flight);
                startActivity(overviewIntent);

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result_by_route, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
