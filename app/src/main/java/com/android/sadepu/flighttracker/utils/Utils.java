package com.android.sadepu.flighttracker.utils;

import com.android.sadepu.flighttracker.model.Airline;
import com.android.sadepu.flighttracker.model.Airport;
import com.android.sadepu.flighttracker.model.Flight;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by S521644 - Sai Chaithanya Adepu on 3/31/2015.
 */
public class Utils {
    public static Calendar getCalFromString(String str, String frmt) {
        SimpleDateFormat format = new SimpleDateFormat(frmt);
        Calendar cal = null;
        try {
            cal = Calendar.getInstance();
            cal.setTime(format.parse(str));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cal;
    }//End

    public static ArrayList<Flight> parseJSONResponseToFlights(JSONObject json) throws JSONException, CloneNotSupportedException {
        ArrayList<Flight> flights = new ArrayList<>();
        JSONArray flightsJSONArray = json.getJSONArray("flightStatuses");

        if (flightsJSONArray.length() != 0) {
            JSONArray airlinesJSONArray = json.getJSONObject("appendix").getJSONArray("airlines");
            Map<String, Airline> airlineMap = new HashMap<>();
            for (int j = 0; j < airlinesJSONArray.length(); j++) {
                JSONObject airlineJSON = airlinesJSONArray.getJSONObject(j);
                Airline airline = new Airline();
                if (airlineJSON.has("fs")) {
                    airline.setFs(airlineJSON.getString("fs"));
                } else {
                    break;
                }
                if (airlineJSON.has("iata"))
                    airline.setIata(airlineJSON.getString("iata"));
                if (airlineJSON.has("name"))
                    airline.setName(airlineJSON.getString("name"));
                airlineMap.put(airline.getFs(), airline);
            }

            JSONArray airportJSONArray = json.getJSONObject("appendix").getJSONArray("airports");
            Map<String, Airport> airportMap = new HashMap<>();
            for (int p = 0; p < airportJSONArray.length(); p++) {
                JSONObject airportJSON = airportJSONArray.getJSONObject(p);
                Airport airport = new Airport();
                if (airportJSON.has("fs")) {
                    airport.setFs(airportJSON.getString("fs"));
                } else {
                    break;
                }
                if (airportJSON.has("city"))
                    airport.setCity(airportJSON.getString("city"));
                if (airportJSON.has("iata"))
                    airport.setIata(airportJSON.getString("iata"));
                if (airportJSON.has("stateCode"))
                    airport.setStateCode(airportJSON.getString("stateCode"));
                if (airportJSON.has("countryCode"))
                    airport.setCountryCode(airportJSON.getString("countryCode"));
                if(airportJSON.has("latitude"))
                    airport.setLatitude(airportJSON.getDouble("latitude"));
                if(airportJSON.has("longitude"))
                    airport.setLongitude(airportJSON.getDouble("longitude"));
                airport.setName(airportJSON.getString("name"));
                airportMap.put(airport.getFs(), airport);
            }

            for (int i = 0; i < flightsJSONArray.length(); i++) {
                JSONObject flightJSON = flightsJSONArray.getJSONObject(i);
                Flight flight = new Flight();
                flight.setFlightId(flightJSON.getLong("flightId"));
                flight.setFlightNumber(flightJSON.getString("flightNumber"));
                flight.setCarrierFsCode(flightJSON.getString("carrierFsCode"));
                flight.setAirline(airlineMap.get(flightJSON.getString("carrierFsCode")));

                flight.setDepartureAirport(airportMap.get(flightJSON.getString("departureAirportFsCode")));
                flight.setArrivalAirport(airportMap.get(flightJSON.getString("arrivalAirportFsCode")));

                flight.setDepartureDate(flightJSON.getJSONObject("departureDate")
                        .getString("dateLocal"));
                JSONObject operationalTimes = flightJSON.getJSONObject("operationalTimes");
                if (operationalTimes.has("estimatedGateDeparture"))
                    flight.setEstimatedGateDeparture(operationalTimes
                            .getJSONObject("estimatedGateDeparture").getString("dateLocal"));
                if (operationalTimes.has("actualGateDeparture"))
                    flight.setActualGateDeparture(operationalTimes
                            .getJSONObject("actualGateDeparture").getString("dateLocal"));

                flight.setArrivalDate(flightJSON.getJSONObject("arrivalDate")
                        .getString("dateLocal"));

                if (operationalTimes.has("estimatedGateArrival"))
                    flight.setEstimatedGateArrival(operationalTimes
                            .getJSONObject("estimatedGateArrival").getString("dateLocal"));
                if (operationalTimes.has("actualGateArrival"))
                    flight.setActualGateArrival(operationalTimes
                            .getJSONObject("actualGateArrival").getString("dateLocal"));


               // if (operationalTimes.has("airportResources")) {

                if (flightJSON.has("airportResources")) {
                    JSONObject airportResources = flightJSON.getJSONObject("airportResources");

                    if (airportResources.has("departureTerminal"))
                        flight.setDepartureTerminal(airportResources.getString("departureTerminal"));
                    if (airportResources.has("departureGate"))
                        flight.setDepartureGate(airportResources.getString("departureGate"));
                    if (airportResources.has("arrivalTerminal"))
                        flight.setArrivalTerminal(airportResources.getString("arrivalTerminal"));
                    if (airportResources.has("arrivalGate"))
                        flight.setArrivalGate(airportResources.getString("arrivalGate"));
                }
               // }


               if (flightJSON.has("delays")) {
                    JSONObject delays = flightJSON.getJSONObject("delays");
                    if (delays.has("departureGateDelayMinutes"))
                        flight.setDepartureGateDelayMinutes(delays.getInt("departureGateDelayMinutes"));
                    if (delays.has("arrivalGateDelayMinutes"))
                        flight.setArrivalGateDelayMinutes(delays.getInt("arrivalGateDelayMinutes"));
               }


                flights.add(flight);
                if (flightJSON.has("codeshares")) {
                    JSONArray codeshares = flightJSON.getJSONArray("codeshares");
                    for (int k = 0; k < codeshares.length(); k++) {
                        Flight clone = (Flight) flight.clone();
                        JSONObject jsonObject = codeshares.getJSONObject(k);
                        clone.setFlightNumber(jsonObject.getString("flightNumber"));
                        clone.setAirline(airlineMap.get(jsonObject.getString("fsCode")));
                        flights.add(clone);
                    }
                }
            }
        }
        return flights;
    }
}
