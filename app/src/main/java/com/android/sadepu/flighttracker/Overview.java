package com.android.sadepu.flighttracker;

import android.app.AlertDialog;
import android.content.ContentValues;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.sadepu.flighttracker.model.Airline;
import com.android.sadepu.flighttracker.model.Airport;
import com.android.sadepu.flighttracker.model.Flight;
import com.android.sadepu.flighttracker.utils.DictionaryOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author S521726
 */
public class Overview extends ActionBarActivity {
    Flight flights;
    TextView departAirport;
    TextView departTime;
    TextView actualTime;
    TextView actualTime1;
    TextView ArrivalAirport;
    TextView arrivalTime;
    SQLiteDatabase sqliteDatabase;


    final SimpleDateFormat totalfrmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    final SimpleDateFormat frmt = new SimpleDateFormat("hh:mm aaa");
    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

    DictionaryOpenHelper doh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);
        final SimpleDateFormat frmt = new SimpleDateFormat("hh:mm aaa");
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Intent intent = getIntent();
        flights = (Flight) intent.getSerializableExtra("flight");
        doh = new DictionaryOpenHelper(getApplicationContext());
        sqliteDatabase = doh.getWritableDatabase();



        Calendar departureDate = flights.getDepartureDate();
        Calendar estimatedGateDeparture = flights.getEstimatedGateDeparture();
        Calendar actualGateDeparture = flights.getActualGateDeparture();
        final float density = getResources().getDisplayMetrics().density;
        final int width = Math.round(30 * density);
        final int height = Math.round(24 * density);
        final Drawable sourceicondrawable = getResources().getDrawable(R.drawable.sourceicon);
        final Drawable destinationicondrawable = getResources().getDrawable(R.drawable.destinationicon);


        sourceicondrawable.setBounds(0, 0, width, height);
        destinationicondrawable.setBounds(0, 0, width, height);

        departAirport = (TextView) findViewById(R.id.departAirport);
        departAirport.setText(flights.getDepartureAirport().toString());
        departAirport.setCompoundDrawables(sourceicondrawable, null, null, null);

        departTime = (TextView) findViewById(R.id.departTime);

        TextView date = (TextView) findViewById(R.id.Date);
        date.setText(dateFormat.format(flights.getDepartureDate().getTime()));


        actualTime = (TextView) findViewById(R.id.actualTime);
        actualTime.setText(frmt.format(flights.getDepartureDate().getTime()));


        if (actualGateDeparture != null) {
            departTime.setText(frmt.format(flights.getActualGateDeparture().getTime()));
        } else if (estimatedGateDeparture != null) {
            departTime.setText(frmt.format(flights.getEstimatedGateDeparture().getTime()));
        } else if (departureDate != null) {
            departTime.setText(frmt.format(flights.getDepartureDate().getTime()));
        }

        actualTime1 = (TextView) findViewById(R.id.actualTimeArrival);
        arrivalTime = (TextView) findViewById(R.id.arrivalTime);

        Calendar estimatedGateArrival = flights.getActualGateArrival();
        Calendar actualGateArrival = flights.getActualGateArrival();
        Calendar ArrivalDate = flights.getArrivalDate();
        if (actualGateArrival != null) {
            arrivalTime.setText(frmt.format(flights.getActualGateArrival().getTime()));
        } else if (estimatedGateDeparture != null) {
            arrivalTime.setText(frmt.format(flights.getEstimatedGateArrival().getTime()));
        } else if (ArrivalDate != null) {
            arrivalTime.setText(frmt.format(flights.getArrivalDate().getTime()));
        }
        TextView airline = (TextView) findViewById(R.id.Airline);
        airline.setText("(" + flights.getAirline().getIata() + ") " + flights.getAirline().getName() + " " + flights.getFlightNumber());
        ArrivalAirport = (TextView) findViewById(R.id.arrivalAirport);
        ArrivalAirport.setText(flights.getArrivalAirport().toString());
        ArrivalAirport.setCompoundDrawables(destinationicondrawable, null, null, null);


        actualTime1.setText(frmt.format(flights.getArrivalDate().getTime()));


        TextView departureGate = (TextView) findViewById(R.id.departGate);
        String gateDeparture = flights.getDepartureGate();
        if (gateDeparture != null) {
            departureGate.setText(gateDeparture);
        } else {
            departureGate.setText("N/A");
        }

        TextView departureTerminal = (TextView) findViewById(R.id.departTerminal);
        String departureTerm = flights.getDepartureTerminal();
        if (departureTerm != null) {
            departureTerminal.setText("Terminal: " + departureTerm);
        } else {
            departureTerminal.setText("Terminal: N/A");
        }

        TextView arrivalGate = (TextView) findViewById(R.id.gateArrival);
        String gateArrival = flights.getArrivalGate();
        if (gateArrival != null) {
            arrivalGate.setText(gateArrival);
        } else {
            arrivalGate.setText("N/A");
        }


        TextView arrivalTerminal = (TextView) findViewById(R.id.arrvTerminal);
        String arrivalTerm = flights.getArrivalTerminal();
        if (arrivalTerm != null) {
            arrivalTerminal.setText("Terminal: " + arrivalTerm);
        } else {
            arrivalTerminal.setText("Terminal: N/A");
        }

        TextView displayTV = (TextView) findViewById(R.id.display);
        int minutes = flights.getDepartureGateDelayMinutes();
        int minutes1 = flights.getArrivalGateDelayMinutes();
        String str = minutes == 1 ? " minute" : " minutes";
        String str1 = minutes1 == 1 ? " minute" : " minutes";
        if (flights.getDepartureGateDelayMinutes() != 0 && flights.getArrivalGateDelayMinutes() != 0) {


            displayTV.setText("Departure Delay in " + minutes + str + "\n" + "Arrival Delay in " + minutes1 + str1);

        } else if (flights.getArrivalGateDelayMinutes() != 0) {
            displayTV.setText("Arrival Delay in " + minutes1 + str1);
        } else if (flights.getDepartureGateDelayMinutes() != 0) {
            displayTV.setText("Departure Delay in " + minutes + str);
        } else {
            displayTV.setText("SCHEDULED - ON TIME");
        }


    }

    public void onClick(View view) {
        String departAirport = this.departAirport.getText().toString();
        String arrivalAirport = this.ArrivalAirport.getText().toString();
        String departTime = this.departTime.getText().toString();
        String arrivalTime = this.arrivalTime.getText().toString();
        String estimatedDepartTime = this.actualTime.getText().toString();
        String estimatedArrivalTime = this.actualTime1.getText().toString();

        ContentValues contentValues = new ContentValues();


        contentValues.put("FID", flights.getFlightId());
        contentValues.put("EstimatedDepartedTime", estimatedDepartTime);
        contentValues.put("EstimatedArrivalTime", estimatedArrivalTime);
        contentValues.put("DepartIataCode", flights.getDepartureAirport().getIata());
        contentValues.put("ArrivalIataCode", flights.getArrivalAirport().getIata());
        contentValues.put("FlightNumber", flights.getFlightNumber());
        contentValues.put("AirlineIata", flights.getAirline().getIata());
        Calendar date = flights.getDepartureDate();
        if (date != null) {
            contentValues.put("DepartureDate", totalfrmt.format(flights.getDepartureDate().getTime()));
        }
        contentValues.put("Airline", flights.getAirline().getName());


        Calendar date1 = flights.getEstimatedGateArrival();
        if (date1 != null) {
            contentValues.put("ArrivalDate", totalfrmt.format(flights.getArrivalDate().getTime()));
        }

        try {

            sqliteDatabase.insert("Trip", null, contentValues);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("This flight is added to the list successfully");
            builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.create().show();
        } catch (Exception sce) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("This flight is already added to the list");
            builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.create().show();//
        }




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.geo) {
            Intent MapsIntent = new Intent(getApplicationContext(), MapsActivity.class);
            MapsIntent.putExtra("flight", flights);
            startActivity(MapsIntent);
            return true;
        } else if (id == R.id.viewMyFlightsOv) {
            Intent intent = new Intent(this, Scheduler.class);
            startActivity(intent);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
}
