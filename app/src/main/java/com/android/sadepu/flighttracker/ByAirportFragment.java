package com.android.sadepu.flighttracker;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.android.sadepu.flighttracker.model.Airline;
import com.android.sadepu.flighttracker.model.Airport;
import com.android.sadepu.flighttracker.utils.RestService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ByAirportFragment extends Fragment {


    private Calendar calendar;
    private SimpleDateFormat dateFormater;
    private List<Airport> airports;
    private AutoCompleteTextView airportACTV;
    private Airport airport;
    private AutoCompleteTextView airlineACTV;
    private List<Airline> airlines;
    private Airline airline;
    private OnSearchByAirportClickListener byAirportClickListener;

    public ByAirportFragment() {
    }

    interface OnSearchByAirportClickListener {
        void onClickByAirport(String url, String mode);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (byAirportClickListener == null) {
            byAirportClickListener = (OnSearchByAirportClickListener) activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_by_airport, container, false);

        final Context context = getActivity().getApplicationContext();
        calendar = Calendar.getInstance();
        dateFormater = new SimpleDateFormat(getResources().getString(R.string.date_formatter));
        final EditText flightDateByAirportET = (EditText) view.findViewById(R.id.flightDateByAirportET);
        flightDateByAirportET.setText(dateFormater.format(calendar.getTime()));
        flightDateByAirportET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(year, monthOfYear, dayOfMonth);
                        flightDateByAirportET.setText(dateFormater.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dpDialog.show();
            }
        });


        airports = new ArrayList<>();
        airportACTV = (AutoCompleteTextView) view.findViewById(R.id.airportACTV);
        airportACTV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                airport = (Airport) parent.getItemAtPosition(position);
            }
        });
        airportACTV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (airportACTV.getRight() - airportACTV.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        airportACTV.setText("");
                        airport = null;
                        return true;
                    }
                }
                return false;
            }
        });


        airlineACTV = (AutoCompleteTextView) view.findViewById(R.id.airlineACTV);
        airlineACTV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                airline = (Airline) parent.getItemAtPosition(position);
            }
        });
        airlineACTV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (airlineACTV.getRight() - airlineACTV.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        airlineACTV.setText("");
                        airline = null;
                        return true;
                    }
                }
                return false;
            }
        });

        Button depBTN = (Button) view.findViewById(R.id.depBTN);
        depBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (airport == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter departure airport name");
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else {
                    String airlineString = airline != null ? airline.toString() : "";
                    String url = RestService.API_URL_MAIN + "/flightstatus/rest/v2/json/airport/status/" + airport.getIata() + "/dep/" +
                            calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH) + "/0" +
                            "?utc=false&numHours=3&maxFlights=100";
                    if (airline != null) {
                        url += "&carrier=" + airline.getIata();
                    }
                    byAirportClickListener.onClickByAirport(url, "To");

                }
            }
        });

        Button arrBTN = (Button) view.findViewById(R.id.arrBTN);
        arrBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (airport == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter arrival airport name");
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else {
                    String url = RestService.API_URL_MAIN + "/flightstatus/rest/v2/json/airport/status/" + airport.getIata() + "/arr/" +
                            calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH) + "/0" +
                            "?utc=false&numHours=3&maxFlights=100";
                    if (airline != null) {
                        url += "&carrier=" + airline.getIata();
                    }
                    byAirportClickListener.onClickByAirport(url, "From");
                }
            }
        });

        Log.d("Fragments DEBUG", "BY AIRPORT FRAGMENT VIEW DONE");
        return view;
    }


    public void setAutocompleteViewAdapters() {
        try {
            FragmentActivity activity = getActivity();
            if (airportACTV != null && activity != null) {
                Context context = activity.getApplicationContext();
                airportACTV.setAdapter(new AutoCompleteAdapter(context, android.R.layout.simple_list_item_1, new ArrayList<Airport>(airports)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setAutocompleteViewAdaptersForAirlines() {
        try {
            FragmentActivity activity = getActivity();
            if (airlineACTV != null && activity != null) {
                Context context = activity.getApplicationContext();
                airlineACTV.setAdapter(new AutoCompleteAdapterForAirlines(context, android.R.layout.simple_list_item_1, new ArrayList<Airline>(airlines)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("airportsByAirport", (ArrayList<? extends android.os.Parcelable>) airports);
        outState.putParcelableArrayList("airlinesByAirport", (ArrayList<? extends android.os.Parcelable>) airlines);
        outState.putParcelable("airportByAirport", airport);
        outState.putParcelable("airlineByAirport", airline);
        outState.putLong("calendar", calendar.getTimeInMillis());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            airports = savedInstanceState.getParcelableArrayList("airportsByAirport");
            airlines = savedInstanceState.getParcelableArrayList("airlinesByAirport");
            airport = savedInstanceState.getParcelable("airportByAirport");
            airline = savedInstanceState.getParcelable("airlineByAirport");
            calendar.setTimeInMillis(savedInstanceState.getLong("calendar"));
            setAutocompleteViewAdapters();
            setAutocompleteViewAdaptersForAirlines();
        } else {
            new AirportAsyncTaskFromFile().execute();
            String url = RestService.API_URL_MAIN + "/airlines/rest/v1/json/active/2015/3/25";
            new AirlinesAsyncTask().execute(url);
        }
    }

    class AirportAsyncTaskFromFile extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... params) {
            airports = new ArrayList<Airport>();
            String filename = "airportDataFile.txt";
            JSONObject json = null;
            InputStream is = null;
            try {
                is = getActivity().getApplicationContext().getAssets().open(filename);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d("ASYNC DEBUG", "Done adding airports from file");
            return RestService.convertStreamToJSON(is);
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                JSONArray jsonArray = json.getJSONArray("airports");
                Log.d("ASYNC DEBUG", "Adding airpots list from file");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Airport airport = new Airport();
                    JSONObject airportJSONObject = jsonArray.getJSONObject(i);
                    airport.setCity(airportJSONObject.getString("city"));
                    airport.setCountryCode(airportJSONObject.getString("countryCode"));
                    airport.setName(airportJSONObject.getString("name"));
                    airport.setIata(airportJSONObject.has("iata") ? airportJSONObject.getString("iata") : "");
                    airport.setCountryName(airportJSONObject.getString("countryName"));
                    airports.add(airport);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setAutocompleteViewAdapters();

        }
    }

    class AirlinesAsyncTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            return RestService.doGet(params[0]);
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            airlines = new ArrayList<>();
            try {
                JSONArray jsonArray = jsonArray = json.getJSONArray("airlines");
                Log.d("ASYNC DEBUG", "Adding airlines list from the response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Airline airline1 = new Airline();
                    JSONObject airlineJsonObject = jsonArray.getJSONObject(i);
                    airline1.setName(airlineJsonObject.getString("name"));
                    airline1.setIata(airlineJsonObject.has("iata") ? airlineJsonObject.getString("iata") : "");
                    airlines.add(airline1);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setAutocompleteViewAdaptersForAirlines();
        }
    }

    class AutoCompleteAdapter extends ArrayAdapter<Airport> implements Filterable {
        List<Airport> originalList;
        List<Airport> resultsSuggestions;

        public AutoCompleteAdapter(Context context, int resource, List<Airport> airportList) {
            super(context, resource, airportList);
            this.originalList = new ArrayList<>(airportList);
        }

        @Override
        public Airport getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView airportTV = (TextView) view.findViewById(android.R.id.text1);
            airportTV.setText(getItem(position).toString());
            return view;
        }

        @Override
        public Filter getFilter() {
            Filter myFilter = new Filter() {

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    resultsSuggestions = new ArrayList<Airport>();
                    if (constraint != null) {
                        if (constraint.toString().length() > 2) {
                            Log.i("DEBUG Filters", "Perform filtering with constraint: " + constraint.toString());
                            List<Airport> temp = new ArrayList<>();
                            for (int i = 0; i < originalList.size(); i++) {
                                if (originalList.get(i).getName().toLowerCase().startsWith(constraint.toString().trim().toLowerCase())
                                        || originalList.get(i).getIata().toLowerCase().startsWith(constraint.toString().trim().toLowerCase())) {
                                    temp.add(originalList.get(i));
                                }
                            }
                            resultsSuggestions = new ArrayList<Airport>(temp);
                        }
                    }
                    Log.i("DEBUG Filters", "Filtered with constraint: " + constraint.toString() + " and got " + resultsSuggestions.size() + " records");
                    FilterResults results = new FilterResults();
                    results.values = resultsSuggestions;
                    results.count = resultsSuggestions.size();
                    return results;
                }

                @Override
                @SuppressWarnings("unchecked")
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    clear();
                    if (constraint != null) {
                        ArrayList<Airport> newValues = (ArrayList<Airport>) results.values;
                        if (newValues != null) {
                            for (int i = 0; i < newValues.size(); i++) {
                                add(newValues.get(i));
                            }
                            if (results.count > 0) {
                                notifyDataSetChanged();
                            } else {
                                notifyDataSetInvalidated();
                            }
                        }
                    }
                }
            };
            return myFilter;
        }
    }

    class AutoCompleteAdapterForAirlines extends ArrayAdapter<Airline> implements Filterable {
        List<Airline> originalList;
        List<Airline> resultsSuggestions;

        public AutoCompleteAdapterForAirlines(Context context, int resource, List<Airline> airlineList) {
            super(context, resource, airlineList);
            this.originalList = new ArrayList<>(airlineList);
        }

        @Override
        public Airline getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView airlineTV = (TextView) view.findViewById(android.R.id.text1);
            airlineTV.setText(getItem(position).toString());
            return view;
        }

        @Override
        public Filter getFilter() {
            Filter myFilter = new Filter() {

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    resultsSuggestions = new ArrayList<Airline>();
                    if (constraint != null) {
                        Log.i("DEBUG Filters", "Perform filtering with constraint: " + constraint.toString());
                        List<Airline> temp = new ArrayList<>();
                        for (int i = 0; i < originalList.size(); i++) {
                            if (originalList.get(i).getName().toLowerCase().startsWith(constraint.toString().trim().toLowerCase())
                                    || originalList.get(i).getIata().toLowerCase().startsWith(constraint.toString().trim().toLowerCase())) {
                                temp.add(originalList.get(i));
                            }
                        }
                        resultsSuggestions = new ArrayList<Airline>(temp);
                    }
                    Log.i("DEBUG Filters", "Filtered with constraint: " + constraint.toString() + " and got " + resultsSuggestions.size() + " records");
                    FilterResults results = new FilterResults();
                    results.values = resultsSuggestions;
                    results.count = resultsSuggestions.size();
                    return results;
                }

                @Override
                @SuppressWarnings("unchecked")
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    clear();
                    if (constraint != null) {
                        ArrayList<Airline> newValues = (ArrayList<Airline>) results.values;
                        if (newValues != null) {
                            for (int i = 0; i < newValues.size(); i++) {
                                add(newValues.get(i));
                            }
                            if (results.count > 0) {
                                notifyDataSetChanged();
                            } else {
                                notifyDataSetInvalidated();
                            }
                        }
                    }
                }
            };
            return myFilter;
        }
    }
}
