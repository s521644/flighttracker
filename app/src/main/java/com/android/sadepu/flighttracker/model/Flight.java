package com.android.sadepu.flighttracker.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.android.sadepu.flighttracker.utils.Utils;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by S521644 - Sai Chaithanya Adepu on 3/31/2015.
 */
public class Flight implements Serializable, Cloneable {
    private static final String FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ss.SSS"; //date format

    private long flightId;
    private String carrierFsCode;
    private String flightNumber;
    private Airline airline;

    private Airport departureAirport;
    private Airport arrivalAirport;

    private Calendar departureDate;
    private Calendar estimatedGateDeparture;
    private Calendar actualGateDeparture;

    private Calendar arrivalDate;
    private Calendar estimatedGateArrival;
    private Calendar actualGateArrival;

    private String departureTerminal;
    private String departureGate;
    private String arrivalTerminal;
    private String arrivalGate;

    private int departureGateDelayMinutes;
    private int arrivalGateDelayMinutes;

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public Airport getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(Airport departureAirport) {
        this.departureAirport = departureAirport;
    }

    public Airport getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(Airport arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public long getFlightId() {
        return flightId;
    }

    public void setFlightId(long flightId) {
        this.flightId = flightId;
    }

    public String getCarrierFsCode() {
        return carrierFsCode;
    }

    public void setCarrierFsCode(String carrierFsCode) {
        this.carrierFsCode = carrierFsCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }


    public Calendar getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = Utils.getCalFromString(departureDate, FORMAT_STRING);
    }

    public Calendar getEstimatedGateDeparture() {
        return estimatedGateDeparture;
    }

    public void setEstimatedGateDeparture(String estimatedGateDeparture) {
        this.estimatedGateDeparture = Utils.getCalFromString(estimatedGateDeparture, FORMAT_STRING);
    }

    public Calendar getActualGateDeparture() {
        return actualGateDeparture;
    }

    public void setActualGateDeparture(String actualGateDeparture) {
        this.actualGateDeparture = Utils.getCalFromString(actualGateDeparture, FORMAT_STRING);
    }

    public Calendar getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = Utils.getCalFromString(arrivalDate, FORMAT_STRING);
    }

    public Calendar getEstimatedGateArrival() {
        return estimatedGateArrival;
    }

    public void setEstimatedGateArrival(String estimatedGateArrival) {
        this.estimatedGateArrival = Utils.getCalFromString(estimatedGateArrival, FORMAT_STRING);
    }

    public Calendar getActualGateArrival() {
        return actualGateArrival;
    }

    public void setActualGateArrival(String actualGateArrival) {
        this.actualGateArrival = Utils.getCalFromString(actualGateArrival, FORMAT_STRING);
    }

    public String getDepartureTerminal() {
        return departureTerminal;
    }

    public void setDepartureTerminal(String departureTerminal) {
        this.departureTerminal = departureTerminal;
    }

    public String getDepartureGate() {
        return departureGate;
    }

    public void setDepartureGate(String departureGate) {
        this.departureGate = departureGate;
    }

    public String getArrivalTerminal() {
        return arrivalTerminal;
    }

    public void setArrivalTerminal(String arrivalTerminal) {
        this.arrivalTerminal = arrivalTerminal;
    }

    public String getArrivalGate() {
        return arrivalGate;
    }

    public void setArrivalGate(String arrivalGate) {
        this.arrivalGate = arrivalGate;
    }

    public int getDepartureGateDelayMinutes() {
        return departureGateDelayMinutes;
    }

    public void setDepartureGateDelayMinutes(int departureGateDelayMinutes) {
        this.departureGateDelayMinutes = departureGateDelayMinutes;
    }

    public int getArrivalGateDelayMinutes() {
        return arrivalGateDelayMinutes;
    }

    public void setArrivalGateDelayMinutes(int arrivalGateDelayMinutes) {
        this.arrivalGateDelayMinutes = arrivalGateDelayMinutes;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Flight{" + airline + "" + flightNumber + "}";
    }
}
