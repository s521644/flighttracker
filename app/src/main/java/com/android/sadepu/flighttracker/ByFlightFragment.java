package com.android.sadepu.flighttracker;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.sadepu.flighttracker.model.Airline;
import com.android.sadepu.flighttracker.utils.RestService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ByFlightFragment extends Fragment {


    private Calendar calendar;
    private SimpleDateFormat dateFormater;
    private AutoCompleteTextView airlineACTV;
    private EditText flightNumberET;
    private List<Airline> airlines;
    private Airline airline;

    private OnSearchByFlightClickListener byFlightClickListener;

    interface OnSearchByFlightClickListener {
        void onSearchClick(String url);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (byFlightClickListener == null) {
            byFlightClickListener = (OnSearchByFlightClickListener) activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_by_flight, container, false);

        flightNumberET = (EditText) view.findViewById(R.id.flightNumberET);
        final Context context = getActivity().getApplicationContext();
        calendar = Calendar.getInstance();
        dateFormater = new SimpleDateFormat(getResources().getString(R.string.date_formatter));
        final EditText flightDateByFlightET = (EditText) view.findViewById(R.id.flightDateByFlightET);
        flightDateByFlightET.setText(dateFormater.format(calendar.getTime()));
        flightDateByFlightET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(year, monthOfYear, dayOfMonth);
                        flightDateByFlightET.setText(dateFormater.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dpDialog.show();
            }
        });

        airlineACTV = (AutoCompleteTextView) view.findViewById(R.id.airACTV);
        airlineACTV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                airline = (Airline) parent.getItemAtPosition(position);
            }
        });
        airlineACTV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (airlineACTV.getRight() - airlineACTV.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        airlineACTV.setText("");
                        airline = null;
                        return true;
                    }
                }
                return false;
            }
        });
        flightNumberET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (flightNumberET.getRight() - flightNumberET.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        flightNumberET.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        Button searchByFlightBtn = (Button) view.findViewById(R.id.searchByFlightBTN);
        searchByFlightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String flightNumber = flightNumberET.getText().toString();
                if (airline == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter airline name");
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else if (flightNumber.isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter flight number");
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else {
                    Log.d("Page State - Values Selected ", airline.toString() + " - " + flightNumber + " - " + dateFormater.format(calendar.getTime()));
                    String url = RestService.API_URL_MAIN + "/flightstatus/rest/v2/json/flight/status/" + airline.getIata() + "/" + flightNumber + "/dep/" +
                            calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH) + "?utc=false";
                    byFlightClickListener.onSearchClick(url);
                }
            }
        });


        Log.d("Fragments DEBUG", "BY FLIGHT FRAGMENT VIEW DONE");
        return view;
    }

    public void setAutocompleteViewAdaptersForAirlines() {
        try {
            FragmentActivity activity = getActivity();
            if (airlineACTV != null && activity != null) {
                Context context = activity.getApplicationContext();
                airlineACTV.setAdapter(new AutoCompleteAdapterForAirlines(context, android.R.layout.simple_list_item_1, new ArrayList<Airline>(airlines)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("airlinesByFlight", (ArrayList<? extends android.os.Parcelable>) airlines);
        outState.putParcelable("airlineByAirport", airline);
        outState.putLong("calendar", calendar.getTimeInMillis());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            airlines = savedInstanceState.getParcelableArrayList("airlinesByFlight");
            airline = savedInstanceState.getParcelable("airlineByAirport");
            calendar.setTimeInMillis(savedInstanceState.getLong("calendar"));
            setAutocompleteViewAdaptersForAirlines();
        } else {
            String url = RestService.API_URL_MAIN + "/airlines/rest/v1/json/active/2015/3/25";
            new AirlinesAsyncTask().execute(url);
        }
    }

    class AirlinesAsyncTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            return RestService.doGet(params[0]);
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            airlines = new ArrayList<>();
            try {
                JSONArray jsonArray = jsonArray = json.getJSONArray("airlines");
                Log.d("ASYNC DEBUG", "Adding airlines list from the response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Airline airline1 = new Airline();
                    JSONObject airlineJsonObject = jsonArray.getJSONObject(i);
                    airline1.setName(airlineJsonObject.getString("name"));
                    airline1.setIata(airlineJsonObject.has("iata") ? airlineJsonObject.getString("iata") : "");
                    airlines.add(airline1);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setAutocompleteViewAdaptersForAirlines();
        }
    }

    class AutoCompleteAdapterForAirlines extends ArrayAdapter<Airline> implements Filterable {
        List<Airline> originalList;
        List<Airline> resultsSuggestions;

        public AutoCompleteAdapterForAirlines(Context context, int resource, List<Airline> airlineList) {
            super(context, resource, airlineList);
            this.originalList = new ArrayList<>(airlineList);
        }

        @Override
        public Airline getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView airlineTV = (TextView) view.findViewById(android.R.id.text1);
            airlineTV.setText(getItem(position).toString());
            return view;
        }

        @Override
        public Filter getFilter() {
            Filter myFilter = new Filter() {

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    resultsSuggestions = new ArrayList<Airline>();
                    if (constraint != null) {
                        Log.i("DEBUG Filters", "Perform filtering with constraint: " + constraint.toString());
                        List<Airline> temp = new ArrayList<>();
                        for (int i = 0; i < originalList.size(); i++) {
                            if (originalList.get(i).getName().toLowerCase().startsWith(constraint.toString().trim().toLowerCase())
                                    || originalList.get(i).getIata().toLowerCase().startsWith(constraint.toString().trim().toLowerCase())) {
                                temp.add(originalList.get(i));
                            }
                        }
                        resultsSuggestions = new ArrayList<Airline>(temp);
                    }
                    Log.i("DEBUG Filters", "Filtered with constraint: " + constraint.toString() + " and got " + resultsSuggestions.size() + " records");
                    FilterResults results = new FilterResults();
                    results.values = resultsSuggestions;
                    results.count = resultsSuggestions.size();
                    return results;
                }

                @Override
                @SuppressWarnings("unchecked")
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    clear();
                    if (constraint != null) {
                        ArrayList<Airline> newValues = (ArrayList<Airline>) results.values;
                        if (newValues != null) {
                            for (int i = 0; i < newValues.size(); i++) {
                                add(newValues.get(i));
                            }
                            if (results.count > 0) {
                                notifyDataSetChanged();
                            } else {
                                notifyDataSetInvalidated();
                            }
                        }
                    }
                }
            };
            return myFilter;
        }
    }
}
