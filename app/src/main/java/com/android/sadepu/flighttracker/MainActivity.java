package com.android.sadepu.flighttracker;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.android.sadepu.flighttracker.model.Airline;
import com.android.sadepu.flighttracker.model.Airport;
import com.android.sadepu.flighttracker.model.Flight;
import com.android.sadepu.flighttracker.utils.RestService;
import com.android.sadepu.flighttracker.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends FragmentActivity implements ActionBar.TabListener,
        ByRouteFragment.OnSearchByRouteClickListener, ByAirportFragment.OnSearchByAirportClickListener,
        ByFlightFragment.OnSearchByFlightClickListener {

    ActionBar actionBar;
    ViewPager viewPager;

//    public static List<Airport> airports;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.mainVP);
        viewPager.setAdapter(new VPAdapter(getSupportFragmentManager()));
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                actionBar.setSelectedNavigationItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        actionBar.addTab(actionBar.newTab().setText("By Route").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("By Airport").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("By Flight").setTabListener(this));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.viewMyFlights) {
            Intent intent = new Intent(this, Scheduler.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onSearchByRoute(String url) {
        new FlightByRouteAsync().execute(url);
    }

    @Override
    public void onClickByAirport(String url, String mode) {
        new FlightByAirportAsync().execute(url, mode);
    }

    @Override
    public void onSearchClick(String url) {
        new FlightByNumberAsync().execute(url);
    }


    class FlightByNumberAsync extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MainActivity.this);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage("Fetching flight data..");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            return RestService.doGet(params[0]);
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                if (json.has("error")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(json.getJSONObject("error").getString("errorMessage"));
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else {
                    ArrayList<Flight> flights = Utils.parseJSONResponseToFlights(json);
                    if (flights.size() != 0) {
                        Intent intent = new Intent(MainActivity.this, Overview.class);
                        intent.putExtra("flight", flights.get(0));
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("There are no flights scheduled!");
                        builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.create().show();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }

        }


    }

    class FlightByRouteAsync extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MainActivity.this);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage("Fetching flights..");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            return RestService.doGet(params[0]);
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                if (json.has("error")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(json.getJSONObject("error").getString("errorMessage"));
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else {
                    ArrayList<Flight> flights = Utils.parseJSONResponseToFlights(json);
                    if (flights.size() != 0) {
                        Intent intent = new Intent(MainActivity.this, ResultByRoute.class);
                        intent.putExtra("flights", flights);
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("There are no flights scheduled!");
                        builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.create().show();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }

        }


    }

    class FlightByAirportAsync extends AsyncTask<String, Void, JSONObject> {

        private String mode;
        private String airlineIata;
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MainActivity.this);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage("Fetching flights..");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            this.mode = params[1];

            return RestService.doGet(params[0]);
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                if (json.has("error")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(json.getJSONObject("error").getString("errorMessage"));
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else {
                    ArrayList<Flight> flights = Utils.parseJSONResponseToFlights(json);
                    if (flights.size() != 0) {
                        Intent intent = new Intent(MainActivity.this, ResultByAirport.class);
                        intent.putExtra("flights", flights);
                        intent.putExtra("mode", mode);
                        intent.putExtra("airlineIata", mode);
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("There are no flights scheduled!");
                        builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.create().show();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }

        }


    }
}

class VPAdapter extends FragmentPagerAdapter {

    public VPAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment f = null;
        if (i == 0) {
            f = new ByRouteFragment();
        } else if (i == 1) {
            f = new ByAirportFragment();
        } else if (i == 2) {
            f = new ByFlightFragment();
        }
        return f;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
