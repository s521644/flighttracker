package com.android.sadepu.flighttracker.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by S521695 on 4/4/2015.
 */
public class DictionaryOpenHelper extends SQLiteOpenHelper {
    private static final String TABLE_CREATE =
            "CREATE TABLE Trip(FID integer primary key, EstimatedDepartedTime TEXT , EstimatedArrivalTime TEXT , DepartIataCode TEXT , ArrivalIataCode TEXT , FlightNumber TEXT , DepartureDate TEXT , Airline TEXT,ArrivalDate TEXT , AirlineIata TEXT) ;";

     public DictionaryOpenHelper(Context context) {
        super(context, "FlightTracker", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(TABLE_CREATE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }


}
