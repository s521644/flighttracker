package com.android.sadepu.flighttracker;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.sadepu.flighttracker.model.Airline;
import com.android.sadepu.flighttracker.model.Airport;
import com.android.sadepu.flighttracker.model.Flight;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * @author s521702
 */
public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    Flight flight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Intent intent = getIntent();
        flight = (Flight) intent.getSerializableExtra("flight");
        Log.d("DEBUG SET UP MAP+=========>>>>>>>", "----");
        setUpMapIfNeeded();
        Airport departureAirport = flight.getDepartureAirport();
        String depAirport=departureAirport.getIata();
        Airport arrivalAirport = flight.getArrivalAirport();
        String arrAirport= arrivalAirport.getIata();
        String airways=flight.getAirline().getName();
        TextView airline=(TextView)findViewById(R.id.gmairlinesTV);
        TextView route=(TextView)findViewById(R.id.routeTV);
        route.setText("Route : "+depAirport+" - "+arrAirport);
        airline.setText("Airline : "+airways);

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            //
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // mMap.setMyLocationEnabled(true);
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();

            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        Airport departureAirport = flight.getDepartureAirport();
        final double departureAirportLatitude = departureAirport.getLatitude();
        double departureAirportLongitude = departureAirport.getLongitude();
        Airport arrivalAirport = flight.getArrivalAirport();
        final double arrivalAirportLatitude = arrivalAirport.getLatitude();
        double arrivalAirportLongitude = arrivalAirport.getLongitude();
        // Log.d("DEBUG SET UP MAP+=========>>>>>>>", departureAirportLatitude + "----" + departureAirportLongitude + "----" + arrivalAirportLatitude + "----" + arrivalAirportLongitude + "----");
        final LatLng depPostion = new LatLng(departureAirportLatitude, departureAirportLongitude);
        final LatLng arrPosition = new LatLng(arrivalAirportLatitude, arrivalAirportLongitude);

        MarkerOptions depMarker = new MarkerOptions().position(depPostion).title(departureAirport.getIata());
        MarkerOptions arrMarker = new MarkerOptions().position(arrPosition).title(arrivalAirport.getIata());
        mMap.addMarker(depMarker.snippet(departureAirport.getName()));
        mMap.addMarker(arrMarker.snippet(arrivalAirport.getName()));

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLng center = null;
                if (departureAirportLatitude < arrivalAirportLatitude) {
                    center = new LatLngBounds(depPostion, arrPosition).getCenter();
                } else {
                    center = new LatLngBounds(arrPosition, depPostion).getCenter();
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 6)); //
            }
        });

        Polygon polygon = mMap.addPolygon(new PolygonOptions()
                        .add(new LatLng(departureAirportLatitude, departureAirportLongitude), new LatLng(arrivalAirportLatitude, arrivalAirportLongitude), new LatLng(arrivalAirportLatitude, arrivalAirportLongitude))
                        .strokeColor(Color.WHITE)
        );

    }
}
