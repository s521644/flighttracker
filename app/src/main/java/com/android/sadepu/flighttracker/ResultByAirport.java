package com.android.sadepu.flighttracker;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.sadepu.flighttracker.model.Airport;
import com.android.sadepu.flighttracker.model.Flight;
import com.android.sadepu.flighttracker.utils.RestService;
import com.android.sadepu.flighttracker.utils.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class ResultByAirport extends ActionBarActivity {
    ArrayList<Flight> flights;
    private String mode;
    ArrayAdapter<Flight> adapter;
    private Calendar calendar;
    private Airport byAirpotHeadTVAirport;
    ListView byAirportLV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final SimpleDateFormat frmt = new SimpleDateFormat("hh:mm aaa");
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_by_airport);

        Intent intent = getIntent();
        flights = (ArrayList<Flight>) intent.getSerializableExtra("flights");
        mode = intent.getStringExtra("mode");
        byAirportLV = (ListView) findViewById(R.id.byAirportLV);
        final LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View headerView = inflater.inflate(R.layout.result_by_airport_header, null);

        final float density = getResources().getDisplayMetrics().density;
        final int width = Math.round(30 * density);
        final int height = Math.round(24 * density);
//        final Drawable sourceicondrawable = getResources().getDrawable(R.drawable.sourceicon);
//        final Drawable destinationicondrawable = getResources().getDrawable(R.drawable.destinationicon);


        TextView byAirpotHeadTV = (TextView) headerView.findViewById(R.id.byAirpotHeadTV);
        TextView dateByAirpot = (TextView) headerView.findViewById(R.id.dateByAirpot);
        if (mode.equalsIgnoreCase("to")) {
            calendar = flights.get(0).getDepartureDate();
            byAirpotHeadTVAirport = flights.get(0).getDepartureAirport();
            byAirpotHeadTV.setText(byAirpotHeadTVAirport.toString());
            dateByAirpot.setText(dateFormat.format(flights.get(0).getDepartureDate().getTime()));

        } else {
            calendar = flights.get(0).getArrivalDate();
            byAirpotHeadTVAirport = flights.get(0).getArrivalAirport();
            byAirpotHeadTV.setText(byAirpotHeadTVAirport.toString());
            dateByAirpot.setText(dateFormat.format(flights.get(0).getArrivalDate().getTime()));
        }
//        byAirpotHeadTV.setCompoundDrawables(sourceicondrawable, null, null, null);

        byAirportLV.addHeaderView(headerView, null, false);
        buildListView();

        Spinner timeSpinner = (Spinner) findViewById(R.id.timeSpinner);
        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String next = String.valueOf(position * 3);
                String dora = mode.equalsIgnoreCase("to") ? "dep" : "arr";
                String url = RestService.API_URL_MAIN + "/flightstatus/rest/v2/json/airport/status/"
                        + byAirpotHeadTVAirport.getIata() + "/" + dora + "/" + calendar.get(Calendar.YEAR)
                        + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH)
                        + "/" + next + "?utc=false&numHours=3&maxFlights=100";
                new FlightByAirportAsync().execute(url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        byAirportLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView delayed1TV = (TextView) view.findViewById(R.id.delayed1TV);
                Flight flight = flights.get(position - 1);
                Log.d("SELECTED FLIGHT->" + (position - 1), flights.get(position - 1).toString());
                Intent overviewIntent = new Intent(getApplicationContext(), Overview.class);
                overviewIntent.putExtra("flight", flight);
                startActivity(overviewIntent);
            }
        });
    }

    class FlightByAirportAsync extends AsyncTask<String, Void, JSONObject> {

        private String mode;
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ResultByAirport.this);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage("Fetching flights..");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            return RestService.doGet(params[0]);
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                if (json.has("error")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                    builder.setMessage(json.getJSONObject("error").getString("errorMessage"));
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else {
                    flights = Utils.parseJSONResponseToFlights(json);
                    if (flights.size() == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                        builder.setMessage("There are no flights scheduled at the time interval selected!");
                        builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.create().show();
                    }
                    buildListView();
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }

        }


    }

    private void buildListView() {
        final SimpleDateFormat frmt = new SimpleDateFormat("hh:mm aaa");
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

        final float density = getResources().getDisplayMetrics().density;
        final int width = Math.round(30 * density);
        final int height = Math.round(24 * density);
        adapter = new ArrayAdapter<Flight>(this, R.layout.result_by_airport_list_item, R.id.airline_list_item_by_airport, flights) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                Flight item = getItem(position);
                View listview = super.getView(position, convertView, parent);
                TextView airline = (TextView) listview.findViewById(R.id.airline_list_item_by_airport);
                airline.setText(item.getAirline().toString() + " " + item.getFlightNumber());
                TextView toorfromAirportTV = (TextView) listview.findViewById(R.id.toorfromAirportTV);
                if (mode.equalsIgnoreCase("to")) {
                    toorfromAirportTV.setText(mode + " " + item.getArrivalAirport().toString());
                } else {
                    toorfromAirportTV.setText(mode + " " + item.getDepartureAirport().toString());
                }
                TextView depiconTV = (TextView) listview.findViewById(R.id.depiconTV_by_airport);
                depiconTV.setText(frmt.format(item.getDepartureDate().getTime()));
                TextView arriconTV = (TextView) listview.findViewById(R.id.arriconTV_by_airport);
                arriconTV.setText(frmt.format(item.getArrivalDate().getTime()));

                final Drawable depicon = getResources().getDrawable(R.drawable.depicon);
                final Drawable arricon = getResources().getDrawable(R.drawable.arricon);

                depicon.setBounds(0, 0, width, height);
                arricon.setBounds(0, 0, width, height);


                depiconTV.setCompoundDrawables(depicon, null, null, null);
                arriconTV.setCompoundDrawables(arricon, null, null, null);

                TextView delayed1TV = (TextView) listview.findViewById(R.id.delayed1TV_by_airport);
                Calendar departureDate = item.getDepartureDate();
                Calendar estimatedGateDeparture = item.getEstimatedGateDeparture();
                Calendar actualGateDeparture = item.getActualGateDeparture();
                Log.d("DATES DEBUG", "Departure Date - " + frmt.format(departureDate.getTime()));

                if (estimatedGateDeparture != null) {
                    Log.d("DATES DEBUG", "Estimated Departure Date - " + frmt.format(estimatedGateDeparture.getTime()));
                    long diff = estimatedGateDeparture.getTime().getTime() - departureDate.getTime().getTime();
                    long diffMinutes = diff / (60 * 1000) % 60;
                    if (diffMinutes > 0) {
                        delayed1TV.setVisibility(View.VISIBLE);
                    }
                } else if (actualGateDeparture != null) {
                    Log.d("DATES DEBUG", "Actual Departure Date - " + frmt.format(actualGateDeparture.getTime()));
                }
                return listview;
            }
        };
        byAirportLV.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result_by_airport, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
