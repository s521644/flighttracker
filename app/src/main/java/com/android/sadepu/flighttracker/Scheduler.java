package com.android.sadepu.flighttracker;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.sadepu.flighttracker.model.Airline;
import com.android.sadepu.flighttracker.model.Airport;
import com.android.sadepu.flighttracker.model.Flight;
import com.android.sadepu.flighttracker.utils.DictionaryOpenHelper;
import com.android.sadepu.flighttracker.utils.RestService;
import com.android.sadepu.flighttracker.utils.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class Scheduler extends ActionBarActivity {
    ListView scheduleLV;
    SQLiteDatabase tripDB;
    DictionaryOpenHelper dictionaryOH;
    Cursor results;
    ArrayList<Flight> flights = new ArrayList<Flight>();
    final SimpleDateFormat frmt = new SimpleDateFormat("hh:mm aaa");
    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheduler);
        Intent intent = getIntent();
        scheduleLV = (ListView) findViewById(R.id.scheduleLV);
        TextView noUserTripsTV = (TextView) findViewById(R.id.noUserTripsTV);
        //TextView userTripsTV = (TextView) findViewById(R.id.userTripsTV);
        results = initializeDatabase();
        results.moveToFirst();

        final float density = getResources().getDisplayMetrics().density;
        final int width = Math.round(30 * density);
        final int height = Math.round(24 * density);

        while (!results.isAfterLast()) {
            String estimatedDepartedTime = results.getString(results.getColumnIndex("EstimatedDepartedTime"));
            String estimatedArrivalTime = results.getString(results.getColumnIndex("EstimatedArrivalTime"));
            String departIataCode = results.getString(results.getColumnIndex("DepartIataCode"));
            String arrivalIataCode = results.getString(results.getColumnIndex("ArrivalIataCode"));
            String airLineName = results.getString(results.getColumnIndex("Airline"));
            String flightNumber = results.getString(results.getColumnIndex("FlightNumber"));
            String departureDate = results.getString(results.getColumnIndex("DepartureDate"));
            String arrivalDate = results.getString(results.getColumnIndex("ArrivalDate"));
            String airLineIata = results.getString(results.getColumnIndex("AirlineIata"));
            Flight flight = new Flight();
            flight.setEstimatedGateDeparture(estimatedDepartedTime);
            Airport deptAirport = new Airport();
            deptAirport.setIata(departIataCode);
            flight.setDepartureAirport(deptAirport);
            flight.getDepartureAirport().getIata();
            Airport arrAirport = new Airport();
            arrAirport.setIata(arrivalIataCode);
            flight.setArrivalAirport(arrAirport);
            flight.getArrivalAirport().getIata();
            Airline airLine = new Airline();
            airLine.setName(airLineName);
            airLine.setIata(airLineIata);
            flight.setAirline(airLine);
            flight.setFlightNumber(flightNumber);
            flight.setDepartureDate(departureDate);
            flight.setArrivalDate(arrivalDate);
            flights.add(flight);
            results.moveToNext();
        }
        if (results != null && !results.isClosed()) {
            results.close();

        }
        if (tripDB != null) {
            tripDB.close();
        }
        if (results.getCount() < 0) {
            Toast.makeText(this, "No User Trips", Toast.LENGTH_SHORT).show();

        }
        if (flights.size() > 0) {

            noUserTripsTV.setVisibility(View.GONE);

        }
        ArrayAdapter adapter = new FlightArrayAdapter(this, R.layout.result_by_schedule_list, R.id.dateTV, flights);
        scheduleLV.setAdapter(adapter);
        scheduleLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Flight flight = flights.get(position);
                String url = RestService.API_URL_MAIN + "/flightstatus/rest/v2/json/flight/status/" + flight.getAirline().getIata() + "/" + flight.getFlightNumber() + "/dep/" +
                        flight.getDepartureDate().get(Calendar.YEAR) + "/" + (flight.getDepartureDate().get(Calendar.MONTH) + 1) + "/" + flight.getDepartureDate().get(Calendar.DAY_OF_MONTH) + "?utc=false";
                new FlightByNumberAsync().execute(url);
            }
        });
    }

    class FlightArrayAdapter extends ArrayAdapter<Flight> {
        final float density = getResources().getDisplayMetrics().density;
        final int width = Math.round(30 * density);
        final int height = Math.round(24 * density);

        public FlightArrayAdapter(Context context, int resource, int rid, List<Flight> flights) {
            super(context, resource, rid, flights);
        }

        @Override
        public View getView(int position, View oldView, ViewGroup parent) {
            View view = super.getView(position, oldView, parent);
            Flight flight = getItem(position);
            TextView dateTV = (TextView) view.findViewById(R.id.dateTV);
            dateTV.setText(dateFormat.format(flight.getDepartureDate().getTime()));
            TextView flightTV = (TextView) view.findViewById(R.id.flightTV);
            flightTV.setText(flight.getAirline().toString() + " " + flight.getFlightNumber());
            final Drawable depicon = getResources().getDrawable(R.drawable.depicon);
            final Drawable arricon = getResources().getDrawable(R.drawable.arricon);
            depicon.setBounds(0, 0, width, height);
            arricon.setBounds(0, 0, width, height);
            TextView arrivalTimeTV = (TextView) view.findViewById(R.id.arrivalTimeTV);
            arrivalTimeTV.setText(frmt.format(flight.getArrivalDate().getTime()));
            TextView arrivaliataTV = (TextView) view.findViewById(R.id.arrivalitatTV);
            arrivaliataTV.setText(flight.getArrivalAirport().getIata());
            TextView departTimeTV = (TextView) view.findViewById(R.id.departTimeTV);
            departTimeTV.setText(frmt.format(flight.getDepartureDate().getTime()));
            TextView departiataTV = (TextView) view.findViewById(R.id.departitatTV);
            departiataTV.setText(flight.getDepartureAirport().getIata());
            departTimeTV.setCompoundDrawables(depicon, null, null, null);
            arrivalTimeTV.setCompoundDrawables(arricon, null, null, null);

            return view;
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scheduler, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public Cursor initializeDatabase() {
        dictionaryOH = new DictionaryOpenHelper(this);
        tripDB = dictionaryOH.getReadableDatabase();


        if (tripDB == null)
            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
        else
            results = tripDB.query("Trip", null, null, null, null, null, null);
        return results;
    }

    class FlightByNumberAsync extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Scheduler.this);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage("Fetching flight data..");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            return RestService.doGet(params[0]);
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                if (json.has("error")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Scheduler.this);
                    builder.setMessage(json.getJSONObject("error").getString("errorMessage"));
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else {
                    ArrayList<Flight> flights = Utils.parseJSONResponseToFlights(json);
                    if (flights.size() != 0) {
                        Intent intent = new Intent(Scheduler.this, Overview.class);
                        intent.putExtra("flight", flights.get(0));
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Scheduler.this);
                        builder.setMessage("There are no flights scheduled!");
                        builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.create().show();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }

        }


    }

}
