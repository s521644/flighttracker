package com.android.sadepu.flighttracker.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by S521644 - Sai Chaithanya Adepu on 3/21/2015.
 */
public class Airport implements Serializable, Parcelable {
    private boolean active;
    private String city;
    private String cityCode;
    private String stateCode;
    private int classification;
    private String countryCode;
    private String countryName;
    private String delayIndexUrl;
    private String elevationFeet;
    private String fs;
    private String iata;
    private String icao;
    private double latitude;
    private Calendar localTime;
    private double longitude;
    private String name;
    private String regionName;
    private String timeZoneRegionName;
    private int utcOffsetHours;
    private String weatherUrl;

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public int getClassification() {
        return classification;
    }

    public void setClassification(int classification) {
        this.classification = classification;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getDelayIndexUrl() {
        return delayIndexUrl;
    }

    public void setDelayIndexUrl(String delayIndexUrl) {
        this.delayIndexUrl = delayIndexUrl;
    }

    public String getElevationFeet() {
        return elevationFeet;
    }

    public void setElevationFeet(String elevationFeet) {
        this.elevationFeet = elevationFeet;
    }

    public String getFs() {
        return fs;
    }

    public void setFs(String fs) {
        this.fs = fs;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Calendar getLocalTime() {
        return localTime;
    }

    public void setLocalTime(Calendar localTime) {
        this.localTime = localTime;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getTimeZoneRegionName() {
        return timeZoneRegionName;
    }

    public void setTimeZoneRegionName(String timeZoneRegionName) {
        this.timeZoneRegionName = timeZoneRegionName;
    }

    public int getUtcOffsetHours() {
        return utcOffsetHours;
    }

    public void setUtcOffsetHours(int utcOffsetHours) {
        this.utcOffsetHours = utcOffsetHours;
    }

    public String getWeatherUrl() {
        return weatherUrl;
    }

    public void setWeatherUrl(String weatherUrl) {
        this.weatherUrl = weatherUrl;
    }

    @Override
    public String toString() {
        String airport = "";
        airport += ((iata != "" && iata != null) ? "(" + iata + ") " : "");
        airport += name;
        return airport;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
