package com.android.sadepu.flighttracker.utils;

import android.os.AsyncTask;

import org.json.JSONObject;


/**
 * Created by S521644 - Sai Chaithanya Adepu on 3/21/2015.
 */
public class MyAsyncTask extends AsyncTask<String, Void, JSONObject> {

    @Override
    protected JSONObject doInBackground(String... urls) {
        return RestService.doGet(urls[0]);
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {

    }
}
