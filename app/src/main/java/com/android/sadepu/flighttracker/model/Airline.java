package com.android.sadepu.flighttracker.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by S521644 - Sai Chaithanya Adepu on 3/25/2015.
 */
public class Airline implements Serializable, Parcelable {
    private String name;
    private String iata;
    private String fs;

    public String getFs() {
        return fs;
    }

    public void setFs(String fs) {
        this.fs = fs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    @Override
    public String toString() {
        String airline = "";
        airline += ((iata != "" && iata != null) ? "(" + iata + ") " : "");
        airline += name;
        return airline;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
