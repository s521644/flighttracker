package com.android.sadepu.flighttracker;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.android.sadepu.flighttracker.model.Airport;
import com.android.sadepu.flighttracker.utils.RestService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

import java.text.SimpleDateFormat;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ByRouteFragment extends Fragment {

    private SimpleDateFormat dateFormater;
    private static List<Airport> airports;
    AutoCompleteTextView depACTV;
    AutoCompleteTextView arrACTV;
    AutoCompleteAdapter adapter;
    private Airport depAirport;
    private Airport arrAirport;
    private Calendar calendar;
    private OnSearchByRouteClickListener routeClickListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (routeClickListener == null) {
            routeClickListener = (OnSearchByRouteClickListener) activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Context context = getActivity().getApplicationContext();
        calendar = Calendar.getInstance();
        dateFormater = new SimpleDateFormat(getResources().getString(R.string.date_formatter));
        final View view = inflater.inflate(R.layout.fragment_by_route, container, false);
        final EditText flightDateET = (EditText) view.findViewById(R.id.flightDateET);
        flightDateET.setText(dateFormater.format(calendar.getTime()));
        flightDateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(year, monthOfYear, dayOfMonth);
                        flightDateET.setText(dateFormater.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dpDialog.show();
            }
        });


        airports = new ArrayList<>();
        depACTV = (AutoCompleteTextView) view.findViewById(R.id.depACTV);
        depACTV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                depAirport = (Airport) parent.getItemAtPosition(position);
            }
        });
        depACTV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (depACTV.getRight() - depACTV.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        depACTV.setText("");
                        depAirport = null;
                        return true;
                    }
                }
                return false;
            }
        });

        arrACTV = (AutoCompleteTextView) view.findViewById(R.id.arrACTV);
        arrACTV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                arrAirport = (Airport) parent.getItemAtPosition(position);
            }
        });
        arrACTV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (arrACTV.getRight() - arrACTV.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        arrACTV.setText("");
                        arrAirport = null;
                        return true;
                    }
                }
                return false;
            }
        });


        Button searchBTN = (Button) view.findViewById(R.id.searchBTN);
        searchBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (depAirport == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter departure airport name");
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else if (arrAirport == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter arrival airport name");
                    builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                } else {
                    Log.d("Page State - Values Selected ", depAirport.toString() + " - " + arrAirport.toString() + " - " + dateFormater.format(calendar.getTime()));
                    String url = RestService.API_URL_MAIN + "/flightstatus/rest/v2/json/route/status/" + depAirport.getIata() + "/" + arrAirport.getIata() + "/dep/" +
                            calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH)+1) + "/" + calendar.get(Calendar.DAY_OF_MONTH) + "" +
                            "?hourOfDay=0&utc=false&numHours=24&maxFlights=25";
                    routeClickListener.onSearchByRoute(url);
                }
            }
        });


        return view;
    }

    public void setAutocompleteViewAdapters() {
        try {
            FragmentActivity activity = getActivity();
            if (depACTV != null && arrACTV != null && activity != null) {
                Context context = activity.getApplicationContext();
                adapter = new AutoCompleteAdapter(context, android.R.layout.simple_list_item_1, new ArrayList<Airport>(airports));
                depACTV.setAdapter(adapter);
                AutoCompleteAdapter adapter1 = new AutoCompleteAdapter(context, android.R.layout.simple_list_item_1, new ArrayList<Airport>(airports));
                arrACTV.setAdapter(adapter1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("airports", (ArrayList<? extends android.os.Parcelable>) airports);
        outState.putParcelable("depAirport", depAirport);
        outState.putParcelable("arrAirport", arrAirport);
        outState.putLong("calendar", calendar.getTimeInMillis());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            airports = savedInstanceState.getParcelableArrayList("airports");
            depAirport = savedInstanceState.getParcelable("depAirport");
            arrAirport = savedInstanceState.getParcelable("arrAirport");
            calendar.setTimeInMillis(savedInstanceState.getLong("calendar"));
            setAutocompleteViewAdapters();
        } else {
            new AirportAsyncTaskFromFile().execute();
        }
    }

    class AirportAsyncTaskFromFile extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... params) {
            airports = new ArrayList<Airport>();
            InputStream is = null;
            try {
                is = getActivity().getApplicationContext().getAssets().open("airportDataFile.txt");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d("ASYNC DEBUG", "Done adding airports from file");
            return RestService.convertStreamToJSON(is);
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                JSONArray jsonArray = json.getJSONArray("airports");
                Log.d("ASYNC DEBUG", "Adding airpots list from file");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Airport airport = new Airport();
                    JSONObject airportJSONObject = jsonArray.getJSONObject(i);
                    airport.setCity(airportJSONObject.getString("city"));
                    airport.setCountryCode(airportJSONObject.getString("countryCode"));
                    airport.setName(airportJSONObject.getString("name"));
                    airport.setIata(airportJSONObject.has("iata") ? airportJSONObject.getString("iata") : "");
                    airport.setCountryName(airportJSONObject.getString("countryName"));
                    airports.add(airport);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setAutocompleteViewAdapters();

        }
    }


    class AutoCompleteAdapter extends ArrayAdapter<Airport> implements Filterable {
        List<Airport> originalList;
        List<Airport> resultsSuggestions;

        public AutoCompleteAdapter(Context context, int resource, List<Airport> airportList) {
            super(context, resource, airportList);
            this.originalList = new ArrayList<>(airportList);
        }

        @Override
        public Airport getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView airportTV = (TextView) view.findViewById(android.R.id.text1);
            airportTV.setText(getItem(position).toString());
            return view;
        }

        @Override
        public Filter getFilter() {
            Filter myFilter = new Filter() {

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    resultsSuggestions = new ArrayList<Airport>();
                    if (constraint != null) {
                        if (constraint.toString().length() > 2) {
                            Log.i("DEBUG Filters", "Perform filtering with constraint: " + constraint.toString());
                            List<Airport> temp = new ArrayList<>();
                            for (int i = 0; i < originalList.size(); i++) {
                                if (originalList.get(i).getName().toLowerCase().startsWith(constraint.toString().trim().toLowerCase())
                                        || originalList.get(i).getIata().toLowerCase().startsWith(constraint.toString().trim().toLowerCase())) {
                                    temp.add(originalList.get(i));
                                }
                            }
                            resultsSuggestions = new ArrayList<Airport>(temp);
                        }
                    }
                    Log.i("DEBUG Filters", "Filtered with constraint: " + constraint.toString() + " and got " + resultsSuggestions.size() + " records");
                    FilterResults results = new FilterResults();
                    results.values = resultsSuggestions;
                    results.count = resultsSuggestions.size();
                    return results;
                }

                @Override
                @SuppressWarnings("unchecked")
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    clear();
                    if (constraint != null) {
                        ArrayList<Airport> newValues = (ArrayList<Airport>) results.values;
                        if (newValues != null) {
                            for (int i = 0; i < newValues.size(); i++) {
                                add(newValues.get(i));
                            }
                            if (results.count > 0) {
                                notifyDataSetChanged();
                            } else {
                                notifyDataSetInvalidated();
                            }
                        }
                    }
                }
            };
            return myFilter;
        }
    }

    interface OnSearchByRouteClickListener {
        void onSearchByRoute(String url);
    }
}

